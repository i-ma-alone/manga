<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1.0'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script type='text/javascript'>
			function check(){
				const x = document.getElementById('site-autre')
				if (document.getElementById('site').value == 'autre'){
					x.style.display = ''
				} else {
					x.style.display = 'none'
				}
			}
			function clik(id){
				x = document.getElementsByClassName('chap');
				for (var i = 0; i < x.length; i++) {
					x[i].style.display = "none";
				}
				let ad  = document.getElementById(id);
				ad.style.display = "inline";
			}
		</script>
<?php
$manga = [];
function lineadd(){
	global $manga;
	if(array_key_exists('title', $_POST)){
		$rang = count($manga);
		$manga[$rang]= array();
		$title = $_POST['title'];
		$chapter = $_POST['chapter'];
		$url = $_POST['url'];
		$site = $_POST['site'];
		$type = $_POST['type'];
		if ($site == 'autre') {
			$site = $_POST['site-autre'];
			$manga[$rang]['href'] = $url;
		} elseif ($site == 'manganelo') {
			$manga[$rang]['href'] = "https://{$site}.com/chapter/{$url}/chapter_{$chapter}";	
		} else {
			$manga[$rang]['href'] ="https://{$site}.net/single/{$url}";
			$manga[$rang]['name'] = "{$title} chapter {$chapter}";
		}
		$manga[$rang]['site'] = $site;
		$manga[$rang]['url'] = $url;
		$manga[$rang]['name'] = $title;
		$manga[$rang]['chapter'] = $chapter;
		if ($type == "lu") {
			$manga[$rang]['rank'] = $rang;
			if ($manga[$rang]['rank'] <= 10) {
				$manga[$rang]['title'] = 0;
			} elseif ($manga[$rang]['rank'] == 11) {
				$manga[$rang]['title'] = -5;
			}
			$manga[$rang]['day']= 0;
			$manga[$rang]['week']= 0;
			
			$manga[$rang]['grade'] = 0;
		}
		$manga[$rang]['type']=$type;
	}
	save();
}
function show(){
	global $manga;
	echo "
		<h2>Lus</h2>
		<ul>
		";
	for ($i=0; $i < count($manga); $i++) {
		echo "
        
            <li style='margin-top:10px;'>
                <a style='float:left;' target='_blank' onclick='clik({$i})' href='{$manga[$i]['href']}'>
					{$manga[$i]['name']}
				</a>
                <form id='{$i}' class='chap' style='display:none;' method='post'>
                    <input type='hidden' name='add' value='{$i}'>
                    <input style='margin-left:10px' type='submit' value='+1'>
                </form>
            </li>";
	}
	echo "
		</ul>";
}
function add(){
	global $manga;
	if (array_key_exists('add', $_POST)) {
		$rang = $_POST['add'];
		$manga[$rang]['chapter'] += 1;
		if ($manga[$rang]['rank'] <= 11){
		    $manga[$rang]['title'] += 1;
        }
		$manga[$rang]['href'] = "https://{$manga[$rang]['site']}.com/chapter/{$manga[$rang]['url']}/chapter_{$manga[$rang]['chapter']}";
	}
}
function save(){
	global $manga;
	$file = fopen('save.json', 'w+');
	fwrite($file,json_encode($manga));
	fclose($file);
}
function load(){
	global $manga;
	$file = fopen('save.json', 'r+');
	$manga = json_decode(fread($file,filesize("save.json")),true);
	fclose($file);
}
function update(){
	global $manga;
	for ($i=1; $i < count($manga); $i++) {
		while ($manga[$i]['title'] >= 5 && $manga[$i]['grade'] == 0) {
			$manga[$i]['title'] -= 5;
			$manga[$i]['rank'] -= 1;
			$a = $manga[$i-1];
			$manga[$i-1] = $manga[$i];
			$manga[$i] = $manga[$i-1];
			$manga[$i]['title'] += 1;
		}
		if ($manga[$i]['title'] > 50) {
			$manga[$i]['title'] -= 50;
			$manga[$i]['grade'] += 1;
		}
		while ($manga[$i]['day'] >= 7) {
			if ($manga[$i]['week'] >= 3) {
			} elseif ($manga[$i]['status'] == "END") {
				$manga[$i]['day'] -= 1;
				$manga[$i]['title'] += 1;
			} else {
				$manga[$i]['day'] -= 7;
				$manga[$i]['week'] += 1;
			}
			
		}
		while ($manga[$i]['day'] <= -7) {
			if ($manga[$i]['week'] > -3 && $manga[$i]['status'] == "") {
				$manga[$i]['day'] += 7;
				$manga[$i]['week'] -= 1;
			} else {
				$manga[$i]['day'] += 1;
				$manga[$i]['title'] -= 1;
			}
			$manga[$i]['day'] += 7;
			$manga[$i]['week'] -= 1;
		}
	}
}
$mode = (array_key_exists('mode', $_GET))?$_GET['mode']:'read';
echo "		<title>Manga - {$mode}</title>
	</head>
	<body>
		<form>
			<label for='mode'>mode : </label>
			<select name='mode'>
				<option>read</option>
				<option>add</option>
				<option>manage</option>
			</select>
			<input type='submit' value='ok'>
		</form>
	</body>";
if ($mode == 'add') {
	$del = 'only enter the url part that refer to your manga, for example: on "https://manganelo.com/chapter/martial_peak/chapter_1161" only the "martial_peak" is needed';
	echo "
		<form method='post' action='/manga/' style='text-align:center;margin-top:15%;'>
			<label for='title'>manga title : </label>
			<input type='text' name='title'><br><br>
			<label for='chapter'>last chapter you read : </label>
			<input type='number' step='any' name='chapter'><br><br>
			<label for='site'>site on which you read your manga : </label>
			<select name='site' id='site' onchange='check()' value='manganelo'>
				<option value='manganelo'>manganelo</option>
				<option value='mangaowl'>mangaowl</option>
				<option value='autre'>autre</option>
			</select>
			<div id='site-autre' style='display: none;'><br>
				<label>put it here :</label>
				<input type='text' name='site-autre'>
			</div><br><br>
			<label for='url'>manga url :</label>
			<input type='text' title='{$del}' name='url'><br><br>
			<label for='type'>Status :</label>
			<select name='type'>
				<option value='lu'>lus</option>
				<option value='en_cours'>en cours</option>
				<option value='paused'>paused</option>
				<option value='fin'>terminé</option>
			</select>
			<input type='submit' value='add'>
		</form>
		<script type='text/javascript'>
			check()
		</script>
	</body>";
} elseif ($mode == 'read') {
	load();
	lineadd();
	show();
}
?>
</html>
